package oc.mimic.tinyzipper;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import oc.mimic.tinyzipper.logic.ZipCreateConfig;
import oc.mimic.tinyzipper.logic.Zipper;
import oc.mimic.tinyzipper.param.Params;
import oc.mimic.tinyzipper.param.ParamsFormatter;
import oc.mimic.tinyzipper.util.Logger;
import oc.mimic.tinyzipper.util.Utils;

/**
 * Hlavní třída aplikace.
 *
 * @author mimic
 */
public final class Application {

  private static final String VERSION = "1.3";
  private static final int GEN_PASSWORD_LENGTH = 32;

  public static void main(String... args) {
    var params = new Params();
    var cmd = JCommander.newBuilder().addObject(params).build();
    cmd.setUsageFormatter(new ParamsFormatter(cmd));
    var validParams = false;

    if (args.length > 0) {
      try {
        cmd.parse(args);
        validParams = true;
      } catch (ParameterException e) {
        Logger.error(e.getMessage());
      }
      if (params.isHelp()) {
        cmd.usage();
      }
    } else {
      cmd.usage();
    }
    Logger.slimMode = (params.isSlim() || params.isSilent());

    if (validParams && !params.isHelp()) {
      if (!params.isSlim() && !params.isSilent()) {
        Logger.info("Starting...");
      }
      var config = new ZipCreateConfig();
      config.setPassword(params.getPassword());
      config.setCompressionLevel(params.getCompressionLevel());
      config.setEncryptType(params.getEncryptType());
      config.setComment(params.getComment());

      var zipper = new Zipper();
      zipper.setSilentMode(params.isSilent());
      zipper.setSlimMode(params.isSlim());

      try {
        switch (params.getMode()) {
          case PACK:
            var password = Utils.generatePassword(GEN_PASSWORD_LENGTH);
            if (params.isGeneratePassword()) {
              config.setPassword(password);
            }
            zipper.pack(params.getSource(), params.getDestination(), config);
            if (params.isGeneratePassword()) {
              Logger.slimMode = false;
              Logger.info("Your ZIP file password (" + GEN_PASSWORD_LENGTH + " chars) is:");
              Logger.info("=======================================");
              Logger.info(password);
              Logger.info("=======================================");
              Logger.slimMode = params.isSlim();
            }
            break;

          case UNPACK:
            zipper.unpack(params.getSource(), params.getDestination(), params.getPassword());
            break;
        }
      } catch (Exception e) {
        Logger.error("An unexpected error occurred while processing the ZIP file. Message: {0}", e.getMessage());
      }
    }
    if (!params.isSlim() && !params.isSilent()) {
      Logger.info("Application Tiny Zipper v{0} (by mimic | 2020) was terminated", VERSION);
      System.exit(0);
    }
  }
}
