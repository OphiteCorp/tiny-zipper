package oc.mimic.tinyzipper.logic;

import oc.mimic.tinyzipper.param.type.CompressionLevelType;
import oc.mimic.tinyzipper.param.type.EncryptType;

/**
 * Konfigurace pro vytvoření ZIP souboru.
 *
 * @author mimic
 */
public final class ZipCreateConfig {

  private CompressionLevelType compressionLevel; // null je bez komprese (STORE)
  private EncryptType encryptType;
  private String password;
  private String comment;

  public CompressionLevelType getCompressionLevel() {
    return compressionLevel;
  }

  public void setCompressionLevel(CompressionLevelType compressionLevel) {
    this.compressionLevel = compressionLevel;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public EncryptType getEncryptType() {
    return encryptType;
  }

  public void setEncryptType(EncryptType encryptType) {
    this.encryptType = encryptType;
  }
}
