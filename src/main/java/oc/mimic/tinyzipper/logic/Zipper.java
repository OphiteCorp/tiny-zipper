package oc.mimic.tinyzipper.logic;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.AesKeyStrength;
import net.lingala.zip4j.model.enums.AesVersion;
import net.lingala.zip4j.model.enums.CompressionMethod;
import net.lingala.zip4j.model.enums.EncryptionMethod;
import net.lingala.zip4j.progress.ProgressMonitor;
import oc.mimic.tinyzipper.param.type.CompressionLevelType;
import oc.mimic.tinyzipper.param.type.EncryptType;
import oc.mimic.tinyzipper.util.FilesizeFormatter;
import oc.mimic.tinyzipper.util.Logger;
import oc.mimic.tinyzipper.util.ProgressBar;
import oc.mimic.tinyzipper.util.Utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * Samotná logika pro práci se ZIP soubory.
 *
 * @author mimic
 */
public final class Zipper {

  private static final String ZIP_EXT = ".zip";

  private boolean silentMode;
  private boolean slimMode;

  /**
   * Vytvoří ZIP soubor ze vstupního souboru nebo adresáře.
   *
   * @param inputFile     Vstupní soubor nebo adresář, ze kterého se má vytvořit soubor ZIP.
   *                      Tato hodnota přijde vždy a cíl bude vždy existovat.
   * @param outputZipFile Výstupní ZIP soubor. Může být nevyplněn nebo cíl nemusí existovat
   *                      viz. {@link Zipper#buildOutputPackFile(File, String)}.
   * @param config        Konfigurace pro vytvoření ZIP souboru.
   *
   * @throws ZipException Nastala neočekávaná chyba okolo vytvoření ZIP souboru.
   */
  public void pack(File inputFile, String outputZipFile, ZipCreateConfig config) throws ZipException {
    Logger.info("Preparing to create a ZIP file");

    var outputFile = buildOutputPackFile(inputFile, outputZipFile);
    var zipData = createZipFile(outputFile, config);
    var zipFile = zipData.zipFile;
    var zip = zipFile.getFile();
    var monitor = zipFile.getProgressMonitor();

    Logger.info("Creating ZIP file");

    if (inputFile.isFile()) {
      zipFile.addFile(inputFile, zipData.params);
    } else {
      zipFile.addFolder(inputFile, zipData.params);
    }
    if (!silentMode) {
      progressHandler(monitor, "");
    }
    // komentář je potřeba přidat až zde - zip soubor musí byt už založen na FS
    if (config.getComment() != null) {
      zipFile.setComment(config.getComment());
    }
    Logger.info("Zip file creation succeeded.");
    Logger.info("Path: ''{0}'' ({1})", zip.getPath(), FilesizeFormatter.format(zip.length()));
  }

  /**
   * Rozbalí ZIP soubor do adresáře.
   *
   * @param inputZipFile Vstupní ZIP soubor.
   * @param outputDir    V7stupní adresář. Může být null.
   * @param password     Potencionální heslo. Může být null.
   *
   * @throws ZipException Nastala neočekávaná chyba okolo ZIP souboru.
   */
  public void unpack(File inputZipFile, String outputDir, String password) throws IOException {
    Logger.info("Preparing to unpack a ZIP file");

    var ext = Utils.getFileExtension(inputZipFile.getName());
    if (ext.isEmpty()) {
      inputZipFile = new File(inputZipFile.getAbsolutePath() + ZIP_EXT);

      if (!inputZipFile.exists()) {
        Logger.error("File ''{0}'' does not exist.", inputZipFile);
        return;
      }
    } else {
      if (inputZipFile.isDirectory()) {
        Logger.error("The input ''{0}'' must be a zip file.", inputZipFile);
        return;
      }
      if (!ZIP_EXT.endsWith(ext)) {
        Logger.error("The input ''{0}'' must be a zip file.", inputZipFile);
        return;
      }
    }
    var verifyState = verify(inputZipFile, password);
    if (verifyState != null) {
      Logger.slimMode = false;
      switch (verifyState) {
        case WRONG_PASSWORD:
          Logger.warning("Invalid password.");
          break;

        case CHECKSUM_MISMATCH:
          Logger.warning("The checksum does not match.");
          break;

        case UNKNOWN_COMPRESSION_METHOD:
          Logger.warning("Unknown compression method.");
          break;

        default:
          Logger.error("Error reading zip file header: " + verifyState);
          break;
      }
      Logger.slimMode = slimMode;
      return;
    }
    File output;

    if (outputDir == null) {
      output = inputZipFile.getParentFile();
    } else {
      output = new File(outputDir);
      output.mkdirs();
    }
    Logger.info("Start unpacking a ZIP file");
    var zipFile = createZipFile(inputZipFile, password);
    var monitor = zipFile.getProgressMonitor();
    zipFile.extractAll(output.getAbsolutePath());

    if (!silentMode) {
      progressHandler(monitor, "");
    }
    Logger.info("Extracting ZIP file successfully completed.");
  }

  /**
   * Nastaví tichý režim, kdy budou zobrazeny jen chybové zprávy.
   *
   * @param silentMode True, pokud má být zapnut tichý režim.
   */
  public void setSilentMode(boolean silentMode) {
    this.silentMode = silentMode;
  }

  /**
   * Nastaví slim režim, kdy budou zobrazeny jen některé zprávy.
   *
   * @param slimMode True, pokud má být zapnut slim režim.
   */
  public void setSlimMode(boolean slimMode) {
    this.slimMode = slimMode;
  }

  /**
   * Vytvoří objekt s informacemi o budoucím zip souboru.
   *
   * @param outputZipFile Výstupní zip soubor.
   * @param config        Konfigurace zip souboru.
   *
   * @return Nová instance s informacemi o zip souboru.
   */
  private ZipData createZipFile(File outputZipFile, ZipCreateConfig config) {
    var params = new ZipParameters();

    if (config.getCompressionLevel() != null && config.getCompressionLevel() != CompressionLevelType.STORE) {
      params.setCompressionMethod(CompressionMethod.DEFLATE);
      params.setCompressionLevel(config.getCompressionLevel().getLevelType());
    } else {
      params.setCompressionMethod(CompressionMethod.STORE);
    }
    if (config.getPassword() != null) {
      params.setEncryptFiles(true);

      if (config.getEncryptType() == EncryptType.AES) {
        params.setEncryptionMethod(EncryptionMethod.AES);
        params.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);
        params.setAesVersion(AesVersion.TWO);
      } else {
        params.setEncryptionMethod(EncryptionMethod.ZIP_STANDARD);
      }
    }
    if (outputZipFile.exists()) {
      try {
        if (outputZipFile.delete()) {
          Logger.info("The file ''{0}'' was deleted", outputZipFile.getPath());
        }
      } catch (Exception e) {
        Logger.error("Could not delete current file: {0}", outputZipFile);
        throw e;
      }
    }
    var zipFile = createZipFile(outputZipFile, config.getPassword());
    return new ZipData(zipFile, params);
  }

  /**
   * Handler pro zobrazení progress baru.
   *
   * @param monitor     Monitor průběhu zpracování zip souboru.
   * @param processName Název procesu (baru).
   */
  private void progressHandler(ProgressMonitor monitor, String processName) {
    var progressBar = new ProgressBar();
    var total = monitor.getTotalWork();
    var maxLen = processName.length();

    progressBar.start(total);

    while (monitor.getState() != ProgressMonitor.State.READY) {
      var file = (monitor.getFileName() != null) ? new File(monitor.getFileName()) : null;
      var name = (file != null) ? file.getName() : "";
      var filesize = (file != null) ? FilesizeFormatter.format(file.length()) : "";
      var process = (file != null) ? String.format("%s%s (%s)", processName, name, filesize) : processName;
      var processLen = process.length();

      process += " ".repeat(Math.max(0, maxLen - processLen));
      progressBar.update(monitor.getWorkCompleted(), total, process);

      if (processLen > maxLen) {
        maxLen = processLen;
      }
    }
    progressBar.done(total, maxLen, "Done!");
  }

  /**
   * Vytvoří objekt zip souboru.
   *
   * @param zipPath  Cílová cesta na budoucí zip soubor.
   * @param password Heslo. Může být null.
   *
   * @return Instance zip souboru.
   */
  private ZipFile createZipFile(File zipPath, String password) {
    ZipFile zipFile;

    if (password != null) {
      zipFile = new ZipFile(zipPath, password.toCharArray());
    } else {
      zipFile = new ZipFile(zipPath);
    }
    zipFile.setCharset(StandardCharsets.UTF_8);
    zipFile.setRunInThread(!silentMode);
    return zipFile;
  }

  /**
   * Ověří platnost ZIP souboru.
   *
   * @param inputZipFile Vstupní ZIP soubor.
   * @param password     Heslo k ZIP.
   */
  private ZipException.Type verify(File inputZipFile, String password) throws IOException {
    var zipFile = new ZipFile(inputZipFile);
    if (zipFile.isEncrypted()) {
      zipFile.setPassword(password.toCharArray());
    }
    var fileHeaders = zipFile.getFileHeaders();

    for (var fileHeader : fileHeaders) {
      try (var is = zipFile.getInputStream(fileHeader)) {
        var b = new byte[4 * 4096];
        while (is.read(b) != -1) {
          // nic, protože chceme jen ověřit heslo
        }
      } catch (ZipException e) {
        return e.getType();
      }
    }
    return null;
  }

  /**
   * Sestaví cestu výstupního ZIP souboru.
   *
   * @param inputFile     Vstupní soubor nebo adresář.
   * @param outputZipFile Výstupní soubor, adresář nebo null.
   *
   * @return Výstupní soubor ZIP.
   */
  static File buildOutputPackFile(File inputFile, String outputZipFile) {
    var output = outputZipFile;

    if (inputFile.isDirectory()) {
      if (output == null) {
        // výstupní zip soubor je null
        output = Path.of(inputFile.getAbsolutePath()).resolve(inputFile.getName()).toString() + ZIP_EXT;
      } else {
        var ext = Utils.getFileExtension(output);
        if (ext.isEmpty()) {
          // výstupní zip soubor je adresář
          output = Path.of(output).resolve(inputFile.getName()).toString() + ZIP_EXT;
        } else {
          // výstupní zip soubor je soubor
          output = Utils.removeFileExtension(output) + ZIP_EXT;
        }
      }
    } else {
      var isInputZipFile = ZIP_EXT.endsWith(Utils.getFileExtension(inputFile.getName()).toLowerCase());
      if (output == null) {
        // výstupní zip soubor je null
        var file = Utils.removeFileExtension(inputFile.getName()) + ZIP_EXT;
        output = Path.of(inputFile.getParent()).resolve(file).toString();
      } else {
        var ext = Utils.getFileExtension(output);
        if (ext.isEmpty()) {
          // je potřeba vždy na konec adresáře přidat lomítko (pokud neexistuje)
          if (!output.endsWith("/") && !output.endsWith("\\")) {
            output += "/";
          }
          // výstupní zip soubor je adresář
          output += Utils.removeFileExtension(inputFile.getName()) + ZIP_EXT;
        } else {
          // výstupní zip soubor je soubor
          output = Utils.removeFileExtension(output) + ZIP_EXT;
        }
      }
      // pokud vstupní zip soubor je soubor s příponou zip
      if (isInputZipFile) {
        output += ZIP_EXT;
      }
    }
    return new File(output);
  }

  /**
   * Informace o zip souboru pro další zpracování.
   */
  private static final class ZipData {

    private final ZipFile zipFile;
    private final ZipParameters params;

    private ZipData(ZipFile zipFile, ZipParameters params) {
      this.zipFile = zipFile;
      this.params = params;
    }
  }
}
