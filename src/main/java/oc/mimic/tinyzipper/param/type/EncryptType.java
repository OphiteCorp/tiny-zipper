package oc.mimic.tinyzipper.param.type;

import oc.mimic.tinyzipper.param.IEnumFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * Typ encryptu.
 *
 * @author mimic
 */
public enum EncryptType implements IEnumFormatter {

  STANDARD("std"),
  AES("aes");

  private final String alias;

  EncryptType(String alias) {
    this.alias = alias;
  }

  /**
   * Získá typ encryptu podle aliasu.
   *
   * @param alias Alias.
   *
   * @return Mód aplikace.
   */
  public static EncryptType getbyAlias(String alias) {
    for (var type : values()) {
      if (type.alias.equalsIgnoreCase(alias)) {
        return type;
      }
    }
    return null;
  }

  @Override
  public List<String> getValues() {
    var values = values();
    var list = new ArrayList<String>(values.length);
    for (var v : values) {
      list.add(v.alias.toUpperCase());
    }
    return list;
  }

  public String getAlias() {
    return alias;
  }
}
