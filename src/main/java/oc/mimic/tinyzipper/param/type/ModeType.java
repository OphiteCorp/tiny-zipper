package oc.mimic.tinyzipper.param.type;

import oc.mimic.tinyzipper.param.IEnumFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * Mód aplikace.
 *
 * @author mimic
 */
public enum ModeType implements IEnumFormatter {

  PACK("pack"),
  UNPACK("unpack");

  private final String alias;

  ModeType(String alias) {
    this.alias = alias;
  }

  /**
   * Získá typ módu aplikace podle aliasu.
   *
   * @param alias Alias.
   *
   * @return Mód aplikace.
   */
  public static ModeType getbyAlias(String alias) {
    for (var mode : values()) {
      if (mode.alias.equalsIgnoreCase(alias)) {
        return mode;
      }
    }
    return null;
  }

  @Override
  public List<String> getValues() {
    var values = values();
    var list = new ArrayList<String>(values.length);
    for (var v : values) {
      list.add(v.alias.toUpperCase());
    }
    return list;
  }

  public String getAlias() {
    return alias;
  }
}
