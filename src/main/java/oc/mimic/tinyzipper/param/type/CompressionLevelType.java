package oc.mimic.tinyzipper.param.type;

import net.lingala.zip4j.model.enums.CompressionLevel;
import oc.mimic.tinyzipper.param.IEnumFormatter;
import oc.mimic.tinyzipper.util.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Deflater;

/**
 * Typ úrovně komprese.
 *
 * @author mimic
 */
public enum CompressionLevelType implements IEnumFormatter {

  STORE(0, null),
  FASTEST(CompressionLevel.FASTEST.getLevel(), CompressionLevel.FASTEST),
  FAST(CompressionLevel.FAST.getLevel(), CompressionLevel.FAST),
  NORMAL(CompressionLevel.NORMAL.getLevel(), CompressionLevel.NORMAL),
  MAXIMUM(CompressionLevel.MAXIMUM.getLevel(), CompressionLevel.MAXIMUM),
  ULTRA(Deflater.BEST_COMPRESSION, CompressionLevel.MAXIMUM);

  private final int level;
  private final CompressionLevel cl;

  CompressionLevelType(int level, CompressionLevel cl) {
    this.level = level;
    this.cl = cl;
  }

  /**
   * Získá typ podle úrovně.
   *
   * @param level Úroveň.
   *
   * @return Typ.
   */
  public static CompressionLevelType getbyLevel(int level) {
    for (var type : values()) {
      if (type.level == level) {
        return type;
      }
    }
    if (level > ULTRA.level) {
      return ULTRA;
    }
    return STORE;
  }

  @Override
  public List<String> getValues() {
    var values = values();
    var list = new ArrayList<String>(values.length);
    for (var v : values) {
      list.add(String.format("%s=%s", v.name(), v.level));
    }
    return list;
  }

  public int getLevel() {
    return level;
  }

  public CompressionLevel getLevelType() {
    // hack, protože zip4j neumí lepší kompresi než 7
    if (this == MAXIMUM || this == ULTRA) {
      try {
        var type = CompressionLevel.class.getDeclaredField(CompressionLevel.MAXIMUM.name());
        var field = type.getDeclaringClass().getDeclaredField("level");
        field.setAccessible(true);
        field.set(CompressionLevel.MAXIMUM, level);
      } catch (Exception e) {
        Logger.error("Unexpected error: " + e.getMessage());
      }
    }
    return cl;
  }
}
