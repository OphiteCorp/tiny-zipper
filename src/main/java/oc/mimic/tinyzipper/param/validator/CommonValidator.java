package oc.mimic.tinyzipper.param.validator;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;
import oc.mimic.tinyzipper.param.Params;
import oc.mimic.tinyzipper.param.type.CompressionLevelType;
import oc.mimic.tinyzipper.param.type.EncryptType;
import oc.mimic.tinyzipper.param.type.ModeType;
import oc.mimic.tinyzipper.util.Utils;

import java.io.File;

/**
 * Validátor pro vstupní parametry.
 *
 * @author mimic
 */
public final class CommonValidator implements IParameterValidator {

  @Override
  public void validate(String name, String value) throws ParameterException {
    String msg = null;

    switch (name) {
      case Params.MODE:
        if (!isValidMode(value)) {
          msg = "Invalid application mode: '" + value + "'";
        }
        break;

      case Params.ENCRYPT_TYPE:
        if (!isValidEncryptType(value)) {
          msg = "Invalid encrypt type: '" + value + "'";
        }
        break;

      case Params.SOURCE:
        if (!isValidSource(value)) {
          msg = "Invalid source: '" + value + "'";
        }
        break;

      case Params.DESTINATION:
        if (!isValidDestination(value)) {
          msg = "Invalid destination: '" + value + "'";
        }
        break;

      case Params.COMPRESSION_LEVEL:
        if (!isValidCompressionLevel(value)) {
          msg = "Invalid compression level: '" + value + "'";
        }
        break;

      case Params.PASSWORD:
        if (!isValidPassword(value)) {
          msg = "The password is not valid. The minimum length is 1 character.";
        }
        break;
    }
    if (msg != null) {
      throw new ParameterException(msg);
    }
  }

  private static boolean isValidMode(String value) {
    return (ModeType.getbyAlias(value) != null);
  }

  private static boolean isValidEncryptType(String value) {
    return (EncryptType.getbyAlias(value) != null);
  }

  private static boolean isValidSource(String value) {
    if (Utils.isEmpty(value)) {
      return false;
    }
    var file = new File(value);
    return file.exists();
  }

  private static boolean isValidDestination(String value) {
    if (Utils.isEmpty(value)) {
      return true; // cíl není povinný
    }
    try {
      new File(value); // test, pokud je možné vytvořit File ze vstupního stringu
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  private static boolean isValidCompressionLevel(String value) {
    try {
      var level = Integer.parseInt(value);
      return (level >= CompressionLevelType.STORE.getLevel() && level <= CompressionLevelType.ULTRA.getLevel());
    } catch (Exception e) {
      return false;
    }
  }

  private static boolean isValidPassword(String value) {
    return !Utils.isEmpty(value);
  }
}
