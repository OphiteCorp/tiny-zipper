package oc.mimic.tinyzipper.param;

import java.util.List;

/**
 * Formáter pro enum v nápovědě JCommander.
 *
 * @author mimic
 */
public interface IEnumFormatter {

  /**
   * Získá hodnoty pro enum.
   *
   * @return Hodnoty.
   */
  List<String> getValues();
}
