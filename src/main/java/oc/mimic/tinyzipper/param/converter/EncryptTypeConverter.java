package oc.mimic.tinyzipper.param.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.tinyzipper.param.type.EncryptType;

/**
 * Konverze typu encryptu.
 *
 * @author mimic
 */
public final class EncryptTypeConverter implements IStringConverter<EncryptType> {

  @Override
  public EncryptType convert(String value) {
    return EncryptType.getbyAlias(value);
  }
}
