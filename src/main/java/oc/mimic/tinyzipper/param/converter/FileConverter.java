package oc.mimic.tinyzipper.param.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.tinyzipper.util.Utils;

import java.io.File;

/**
 * Konverze souboru.
 *
 * @author mimic
 */
public final class FileConverter implements IStringConverter<File> {

  @Override
  public File convert(String value) {
    if (!Utils.isEmpty(value)) {
      return new File(value);
    }
    return null;
  }
}
