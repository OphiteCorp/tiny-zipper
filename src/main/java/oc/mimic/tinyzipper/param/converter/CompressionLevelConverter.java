package oc.mimic.tinyzipper.param.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.tinyzipper.param.type.CompressionLevelType;

/**
 * Konverze textu z konzole na úroveň komprese.
 *
 * @author mimic
 */
public final class CompressionLevelConverter implements IStringConverter<CompressionLevelType> {

  @Override
  public CompressionLevelType convert(String value) {
    var level = Integer.parseInt(value);
    return CompressionLevelType.getbyLevel(level);
  }
}
