package oc.mimic.tinyzipper.param.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.tinyzipper.param.type.ModeType;

/**
 * Konverze módu aplikace.
 *
 * @author mimic
 */
public final class ModeTypeConverter implements IStringConverter<ModeType> {

  @Override
  public ModeType convert(String value) {
    return ModeType.getbyAlias(value);
  }
}
