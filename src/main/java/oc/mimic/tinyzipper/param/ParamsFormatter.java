package oc.mimic.tinyzipper.param;

import com.beust.jcommander.DefaultUsageFormatter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterDescription;
import com.beust.jcommander.Strings;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

/**
 * Vlastní formáter.<br>
 * Byl nutný upravit, protože původní má na hovno výpis pro enumy.
 *
 * @author mimic
 */
public final class ParamsFormatter extends DefaultUsageFormatter {

  /**
   * Vytvoření nové instance.
   *
   * @param commander Instance commanderu.
   */
  public ParamsFormatter(JCommander commander) {
    super(commander);
  }

  @Override
  public void appendAllParametersDetails(StringBuilder out, int indentCount, String indent,
          List<ParameterDescription> sortedParameters) {

    if (sortedParameters.size() > 0) {
      out.append(indent).append("  Options:\n");
    }
    for (var pd : sortedParameters) {
      var parameter = pd.getParameter();
      var description = pd.getDescription();
      var hasDescription = !description.isEmpty();

      out.append(indent).append("  ").append(parameter.required() ? "* " : "  ").append(pd.getNames()).append("\n");

      if (hasDescription) {
        wrapDescription(out, indentCount, s(indentCount) + description);
      }
      var def = pd.getDefault();

      if (pd.isDynamicParameter()) {
        var syntax = "Syntax: " + parameter.names()[0] + "key" + parameter.getAssignment() + "value";

        if (hasDescription) {
          out.append(newLineAndIndent(indentCount));
        } else {
          out.append(s(indentCount));
        }
        out.append(syntax);
      }

      if (def != null && !pd.isHelp()) {
        var displayedDef = Strings.isStringEmpty(def.toString()) ? "<empty string>" : def.toString();
        var defaultText = "Default: " + (parameter.password() ? "********" : displayedDef);

        if (hasDescription) {
          out.append(newLineAndIndent(indentCount));
        } else {
          out.append(s(indentCount));
        }
        out.append(defaultText);
      }
      var type = pd.getParameterized().getType();

      if (type.isEnum()) {
        // upravená část
        Collection valueList;
        if (IEnumFormatter.class.isAssignableFrom(type)) {
          Object[] items = EnumSet.allOf((Class<? extends Enum>) type).toArray();
          if (items.length > 0) {
            valueList = ((IEnumFormatter) items[0]).getValues();
          } else {
            valueList = Collections.emptyList();
          }
        } else {
          valueList = EnumSet.allOf((Class<? extends Enum>) type); // původní řádek s tím, že vracel toString()
        }
        // konec upravené části
        var possibleValues = "Possible Values: " + valueList;

        if (!description.contains("Options: " + valueList)) {
          if (hasDescription) {
            out.append(newLineAndIndent(indentCount));
          } else {
            out.append(s(indentCount));
          }
          out.append(possibleValues);
        }
      }
      out.append("\n");
    }
  }

  // metoda, která není v rodiči protected, takže ať žije duplikování kódu
  private static String newLineAndIndent(int indent) {
    return "\n" + s(indent);
  }
}
