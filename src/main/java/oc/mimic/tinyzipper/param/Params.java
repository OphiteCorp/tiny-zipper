package oc.mimic.tinyzipper.param;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.BooleanConverter;
import oc.mimic.tinyzipper.param.converter.CompressionLevelConverter;
import oc.mimic.tinyzipper.param.converter.EncryptTypeConverter;
import oc.mimic.tinyzipper.param.converter.FileConverter;
import oc.mimic.tinyzipper.param.converter.ModeTypeConverter;
import oc.mimic.tinyzipper.param.type.CompressionLevelType;
import oc.mimic.tinyzipper.param.type.EncryptType;
import oc.mimic.tinyzipper.param.type.ModeType;
import oc.mimic.tinyzipper.param.validator.CommonValidator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Parametry aplikace.
 *
 * @author mimic
 */
@Parameters(commandDescription = "Available application parameters", separators = ":")
public final class Params {

  public static final String MODE = "-m";
  public static final String SOURCE = "-s";
  public static final String DESTINATION = "-d";
  public static final String COMPRESSION_LEVEL = "-l";
  public static final String PASSWORD = "-p";
  public static final String COMMENT = "-c";
  public static final String ENCRYPT_TYPE = "-e";
  public static final String GENERATE_PASSWORD = "--genpwd";
  public static final String SLIM = "--slim";
  public static final String SILENT = "--silent";
  public static final String HELP = "--help";

  @Parameter
  private List<String> parameters = new ArrayList<>(); // obsahuje všechny ostatní parametry, které nejsou podporované

  @Parameter(names = MODE,
             order = 1,
             required = true,
             converter = ModeTypeConverter.class,
             validateWith = CommonValidator.class,
             description = "Application mode.")
  private ModeType mode;

  @Parameter(names = SOURCE,
             order = 10,
             required = true,
             converter = FileConverter.class,
             validateWith = CommonValidator.class,
             description = "Input file or directory. For pack mode, there will be a file or directory. For unpack file with .zip extension.")
  private File source;

  @Parameter(names = DESTINATION,
             order = 11,
             validateWith = CommonValidator.class,
             description = "Destination file or directory. For pack mode, there will be a file or directory. For unpack directory where zip file should be unzipped.")
  private String destination;

  @Parameter(names = COMPRESSION_LEVEL,
             order = 20,
             converter = CompressionLevelConverter.class,
             validateWith = CommonValidator.class,
             description = "Compression level.")
  private CompressionLevelType compressionLevel = CompressionLevelType.NORMAL;

  @Parameter(names = ENCRYPT_TYPE,
             order = 21,
             converter = EncryptTypeConverter.class,
             validateWith = CommonValidator.class,
             description = "Encrypt the contents of the zip file.")
  private EncryptType encryptType = EncryptType.AES;

  @Parameter(names = PASSWORD,
             order = 21,
             validateWith = CommonValidator.class,
             description = "Custom password for ZIP file.")
  private String password;

  @Parameter(names = COMMENT,
             order = 22,
             validateWith = CommonValidator.class,
             description = "Comment. It will be displayed on the right when the archive is opened.")
  private String comment;

  @Parameter(names = GENERATE_PASSWORD,
             order = 30,
             converter = BooleanConverter.class,
             validateWith = CommonValidator.class,
             description = "Automatically generates an extra strong password that writes to the console when a ZIP archive is created.")
  private boolean generatePassword;

  @Parameter(names = SLIM,
             order = 31,
             converter = BooleanConverter.class,
             validateWith = CommonValidator.class,
             description = "Enables slim mode. In this mode, only general information is displayed. Everything else is concealed in the console.")
  private boolean slim;

  @Parameter(names = SILENT,
             order = 32,
             converter = BooleanConverter.class,
             validateWith = CommonValidator.class,
             description = "Silent mode. Only potential errors are written to the console.")
  private boolean silent;

  @Parameter(names = HELP, help = true, order = 999, description = "Displays this help.")
  private boolean help;

  public List<String> getParameters() {
    return parameters;
  }

  public ModeType getMode() {
    return mode;
  }

  public File getSource() {
    return source;
  }

  public String getDestination() {
    return destination;
  }

  public CompressionLevelType getCompressionLevel() {
    return compressionLevel;
  }

  public EncryptType getEncryptType() {
    return encryptType;
  }

  public String getPassword() {
    return password;
  }

  public String getComment() {
    return comment;
  }

  public boolean isGeneratePassword() {
    return generatePassword;
  }

  public boolean isSlim() {
    return slim;
  }

  public boolean isHelp() {
    return help;
  }

  public boolean isSilent() {
    return silent;
  }
}
