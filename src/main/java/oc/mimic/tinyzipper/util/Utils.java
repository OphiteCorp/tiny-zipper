package oc.mimic.tinyzipper.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Pomocné metody.
 *
 * @author mimic
 */
public final class Utils {

  /**
   * Získá příponu souboru.
   *
   * @param fileName Vstupní název souboru nebo cesta.
   *
   * @return Přípona souboru. Prázdný text, pokud soubor žádnou příponu nemá nebo se jedná o adresář.
   */
  public static String getFileExtension(String fileName) {
    var lastIndexOf = fileName.lastIndexOf(".");
    if (lastIndexOf == -1) {
      return "";
    }
    var ext = fileName.substring(lastIndexOf);
    if (ext.startsWith(".")) {
      ext = ext.substring(1);
    }
    return ext;
  }

  /**
   * Odebere příponu (typ) souboru z jeho názvu.
   *
   * @param fileName Název souboru nebo cesta.
   *
   * @return Název souboru bez přípony.
   */
  public static String removeFileExtension(String fileName) {
    var index = fileName.lastIndexOf('.');
    return (index > 0) ? fileName.substring(0, fileName.lastIndexOf('.')) : fileName;
  }

  /**
   * Vygeneruje náhodné extra silné heslo obsahující znaky [A-Za-z0-9] + speciální znaky.
   *
   * @param length Délka hesla.
   *
   * @return Náhodné heslo.
   */
  public static String generatePassword(int length) {
    // schválně není " protože se používá v konzoli pro řetězce
    var pattern = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:',<.>/?";
    var patternChars = pattern.toCharArray();
    var password = new char[length];
    var rand = createSecureRandom();

    for (var i = 0; i < length; i++) {
      var n = rand.nextInt(patternChars.length);
      password[i] = patternChars[n];
    }
    // neměl by být potřeba shuffle, ale pro lepší pocit
    for (int i = 0; i < password.length; i++) {
      var n = rand.nextInt(password.length);
      var temp = password[i];
      password[i] = password[n];
      password[n] = temp;
    }
    return new String(password);
  }

  /**
   * Vyhodnotí, zda je řetězec prázdný nebo null.
   *
   * @param value Vstupní řetězec.
   *
   * @return True, pokud je prázdný nebo null.
   */
  public static boolean isEmpty(String value) {
    return (value == null || value.strip().length() == 0);
  }

  /**
   * Vytvoří novou instanci číselného secure random generátoru podle SHA1PRNG.
   *
   * @return Nová instance.
   */
  public static SecureRandom createSecureRandom() {
    try {
      return SecureRandom.getInstance("SHA1PRNG");
    } catch (NoSuchAlgorithmException e) {
      return new SecureRandom();
    }
  }
}
