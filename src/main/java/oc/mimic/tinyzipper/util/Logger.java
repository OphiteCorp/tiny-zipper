package oc.mimic.tinyzipper.util;

import java.text.MessageFormat;

/**
 * Pomocná třída pro logování do konzole.
 *
 * @author mimic
 */
public final class Logger {

  private static final String PREFIX_DEBUG = "DEBUG";
  private static final String PREFIX_INFO = "INFO ";
  private static final String PREFIX_WARNING = "WARN ";
  private static final String PREFIX_ERROR = "ERROR";

  public static boolean slimMode;

  /**
   * Vypíše do konzole debug zprávu.
   *
   * @param message Vstupní zpráva. Může být i formát, kde parametry jsou jako {0}, {1}, ...
   * @param params  Volitelné parametry.
   *
   * @return Vrátí výstupní zprávu.
   */
  public static String debug(String message, Object... params) {
    return log(PREFIX_DEBUG, message, params);
  }

  /**
   * Vypíše do konzole informační zprávu.
   *
   * @param message Vstupní zpráva. Může být i formát, kde parametry jsou jako {0}, {1}, ...
   * @param params  Volitelné parametry.
   *
   * @return Vrátí výstupní zprávu.
   */
  public static String info(String message, Object... params) {
    return log(PREFIX_INFO, message, params);
  }

  /**
   * Vypíše do konzole vyrovnou zprávu.
   *
   * @param message Vstupní zpráva. Může být i formát, kde parametry jsou jako {0}, {1}, ...
   * @param params  Volitelné parametry.
   *
   * @return Vrátí výstupní zprávu.
   */
  public static String warning(String message, Object... params) {
    return log(PREFIX_WARNING, message, params);
  }

  /**
   * Vypíše do konzole chybovou zprávu.
   *
   * @param message Vstupní zpráva. Může být i formát, kde parametry jsou jako {0}, {1}, ...
   * @param params  Volitelné parametry.
   *
   * @return Vrátí výstupní zprávu.
   */
  public static String error(String message, Object... params) {
    return log(PREFIX_ERROR, message, params);
  }

  /**
   * Hlavní metoda pro výpis logu do konzole.
   *
   * @param prefix  Prefix (většinou úroveň logování).
   * @param message Zpráva.
   * @param params  Volitelné parametry.
   *
   * @return Formátovaná zpráva, která se vypsala do konzole.
   */
  private static String log(String prefix, String message, Object... params) {
    var outMsg = message;

    if (params != null && params.length > 0) {
      outMsg = MessageFormat.format(message, params);
    }
    var msg = String.format("[tiny-zipper] [%s] [%s] %s", prefix, Thread.currentThread().getId(), outMsg);

    if (!slimMode || PREFIX_ERROR.equals(prefix)) {
      System.out.println(msg);
    }
    return msg;
  }
}
