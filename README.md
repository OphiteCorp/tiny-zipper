# Tiny Zipper

This app consists of 2 parts:
- **a)** Allows you to create a ZIP archive from a file or directory. The zip archive may contain a comment, password, and some compression level.
- **b)** Extract existing ZIP archive to any directory.

It is a simple console application that is controlled by parameters.

## Download

1) Go to the **Tags** section.
2) Find the latest version of the app.
3) Click the Download button -> Download artifacts -> package-release (it is a zip file, then unzip it).

## Requirements

- Java 11 (JRE or JDK)

## How to use it

Download the application and run it with `java -jar tiny-zipper.jar`. Without any other parameters, the help is automatically displayed.

## Parameters

`-m` = (**required**) Specifies the mode to create or unpack the ZIP archive. Available values: [pack, unpack]\
`-s` = (**required**) Input file or directory. For pack mode, there will be a file or directory. For unpack file with
 .zip extension.\
`-d` = Destination file or directory. For pack mode, there will be a file or directory. For unpack directory where zip file should be unzipped.\
`-l` = Compression level. It can take values of [0, 1, 3, 5, 7, 9], where 0 is without compression and 9 with maximum compression. The default value is 5.\
`-p` = Specifies the password for the archive.\
`-c` = Sets the custom comment that appears after opening the archive.\
`-e` = Encrypt the contents of the zip file. Can be [AES, STD]. The default value is AES (256bit).\
`--genpwd` = Automatically generates an extra strong password for the archive (32 chars). This password is displayed in
 the console after the archive is created.\
`--slim` = Enables slim mode. In this mode, only error messages and progress are displayed.\
`--silent` = Silent mode. Only potential errors are written to the console.\
`--help` = Displays this help.

> All parameters can be entered with a space or a colon as the separator between the key and the value.

## Few examples

**Creates a ZIP archive (the shortest version) - it does not contain compression or password, nothing else**\
`java -jar tiny-zipper.jar -m:pack -s:"c:/my folder/image.png"`
> Creates the "image.zip" archive into the current directory where the tiny-zipper is located.

**Creates the ZIP archive to another directory with maximum compression**\
`java -jar tiny-zipper.jar -m:pack -s:"c:/my folder/image.png" -d:"e:/" -l:9`\
`java -jar tiny-zipper.jar -m:pack -s:"c:/my folder/image.png" -d:"e:/data/" -l:9`\
`java -jar tiny-zipper.jar -m:pack -s:"c:/my folder/image.png" -d:"e:/file" -l:9`\
`java -jar tiny-zipper.jar -m:pack -s:"c:/my folder/image.png" -d:"e:/file.zip" -l:9`\
`java -jar tiny-zipper.jar -m:pack -s:"c:/my folder/image.png" -d:"e:/my lord/image.png" -l:9`
> 1) Creates the file into: e:/image.zip
> 2) Creates the file into: e:/data/image.zip
> 3) Creates the file into: e:/file.zip
> 4) Creates the file into: e:/file.zip
> 5) Creates the file into: e:/my lord/image.png.zip

**Create a ZIP from a folder**\
`java -jar tiny-zipper.jar -m:pack -s:"c:/my folder" -d:"e:/file.zip" -l:9`\

**Add a comment when creating a ZIP archive**\
`java -jar tiny-zipper.jar -m:pack -s:"c:/image.png" -c:"My loooong comment"`

**Change encryption from AES to standard ZIP**\
`java -jar tiny-zipper.jar -m:pack -s:"c:/image.png" -e:std`

**Add your own password**\
`java -jar tiny-zipper.jar -m:pack -s:"c:/image.png" -p:secret`

**Generates an extra strong password**\
`java -jar tiny-zipper.jar -m:pack -s:"c:/image.png" --genpwd`
> When creating an archive, it uses a random 32-character strong password. Such as: **~Pc!T^FtA'x{yD@]MEJpzZW\!q<3~hkV**\
> After creating the archive, write this strong password to the console.

**And finally a bit more complex ZIP archive creation**\
`java -jar tiny-zipper.jar -m:pack -s:"c:/my data" -d:"e:/zips/data" -c:"my archive" -l:9 --genpwd --slim`
> Creates an archive from the "c:/my data" directory and saves the ZIP archive to "e:/zips/data.zip". Add a comment "my archive" to the archive and use the maximum data compression. The password will be generated extra strong and, thanks to slim mode, we will hide info from the console.

**Unpack existing zip archive (shortest version)**\
`java -jar tiny-zipper.jar -m:unpack -s:"c:/my folder/image.zip"`\
`java -jar tiny-zipper.jar -m:unpack -s:"c:/my zipped folder"`

**Alternatively, you can also unpack it into another directory**\
`java -jar tiny-zipper.jar -m:unpack -s:"c:/my zipped folder" -d:"e:/data/"`

**If the archive contains a password, add the -p parameter**\
`java -jar tiny-zipper.jar -m:unpack -s:"c:/my zipped folder" -d:"e:/data/" -p:myPassword`
> There is no need to use other parameters for unpacking. Just the path to ZIP, any output path and password if the archive is password protected.
> If the password contains special characters, enclose the password in quotation marks.

## Recommendation
- Wrap all paths as source and destination in quotation marks.
- Complex passwords that contain special characters wrapped in quotation marks and do not use a quotation mark for a password.
- Compression level set max to 7, otherwise it takes a long time (unless it's a problem).
- Always use AES (default) and complex passwords with a minimum of 8 characters for better security.
- For systems where you need to hide console output completely, use `--slim` or `--silent`.